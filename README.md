# VoiceCV 2.0 specification 

VoiceCV allows a user to ask questions about a CV of a particular person. It is assumed that a user will copy paste his CV to our VoiceCV app. The VoiceCV will generate a snippet of code to include to the user’s personal web page. Visitors will be able to ask CV questions by typing or by voice one by one. The VoiceCV will display the answers. 

Git repository https://gitlab.com/promethistai/VoiceCV includes clients and server API

The VoiceCV consists from four parts:
- https://VoiceCV.promethist.ai **VoiceCV init** web app (client) for creation and  modification of CV 
- **VoiceCV server** part (API)
- **Flowstorm Bot** Bot.flowstorm.ai code is generated for a user to embedded it to a web page
- **Flowstorm application** - dialogue handling the questions and answers

## VoiceCV user’s execution steps

- After clicking on the link of the VoiceCV init app the user needs to login to Flowstorm. (sing up, login.)
- The VoiceCV allows users to enter the unstructured text CV and test sample questions.
- Pressing a “submit” the CV will be saved on the server site and a ID TOKEN is generated.
- The snippet of code is copy pasted to the user’s personal web app.

## VoiceCV init app

We need to create the sing up and log in initial page (Martin) and open the VoiceCV init

## VoiceCV server part will offer two APIs, 

We will use the first server API for initializing and testing. The first API will expect a POST request with JSON containing the ENTITY (name of the CV owner), DOCUMENT (the CV) and a QUESTION. 
After pressing submit in the VoiceCV init application a JSON file will be POSTed to the server.  The JSON will contain ENTITY and CV. The server will generate an ID TOKEN to save the particular content to the Mongo DB or FS. The ID TOKEN will be saved as a user variable in Flowstorm for the signed in user. 
The VoiceCV init application will create a bot snipped of code. It will contain the ID TOKEN to identify the user’s CV data in the Mongo DB or FS. The question answering will run under control of a flowstorm application.

*) we need to add a TOKEN ID parameter to the bot.flowstorm.ai code to identify the CV text. 

##  Flowstorm application

The JS bot.flowstorm.ai will invoke the flowstorm application. Upon the start flowstorm application will load the CV text from Mongo DB or FS and will handle the dialogue. 

## FlowStorm Bot

We need to update the Flowstorm.bot with an additional parameter ID TOKEN to send it over to the server API
